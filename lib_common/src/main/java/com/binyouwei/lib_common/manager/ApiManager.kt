package com.binyouwei.lib_common.manager

import com.binyouwei.lib_common.network.api.ApiInterface

/**
 * @desc   API管理器
 */
object ApiManager {
    val api by lazy { HttpManager.create(ApiInterface::class.java) }
}