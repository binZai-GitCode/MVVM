# MVVM

> 作为一个采用了MVVM架构的项目，在项目中没有使用到DataBinding，我参考了多个使用该架构的项目，大部分没有用到DataBinding，对于MVVM架构项目不适用DataBinding，给的较多的解释是：Compose的时代即将来临，xml编写ui的时代即将一去不复返了，为了避免项目后期用上Compose后，还得重新删除、编写各种ui赋值操作。


# 用到的库
[TheRouter](https://github.com/HuolalaTech/hll-wp-therouter-android)：货拉拉开源的路由框架，针对 Android 平台实现组件化、跨模块调用、动态化等功能的集成框架。

[DataStore](https://developer.android.google.cn/topic/libraries/architecture/datastore?hl=zh-cn)：一种数据存储解决方案，允许您使用协议缓冲区存储键值对或类型化对象。DataStore 使用 Kotlin 协程和 Flow 以异步、一致的事务方式存储数据。
