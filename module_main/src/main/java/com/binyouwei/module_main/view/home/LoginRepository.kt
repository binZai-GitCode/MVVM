package com.binyouwei.module_main.view.home

import com.binyouwei.lib_common.manager.ApiManager
import com.binyouwei.lib_common.model.User
import com.binyouwei.lib_common.network.repository.BaseRepository


/**
 * @desc   登录仓库
 */
class LoginRepository : BaseRepository() {

    /**
     * 登录
     * @param username  用户名
     * @param password  密码
     */
    suspend fun login(username: String, password: String): User? {
        return requestResponse {
            ApiManager.api.login(username, password)
        }
    }
}