package com.binyouwei.mvvm

import android.os.Bundle
import android.view.View
import com.binyouwei.lib_common.base.BaseViewBindActivity
import com.binyouwei.lib_common.network.constant.HomeActivity
import com.binyouwei.lib_common.utils.DataStoreUtils
import com.binyouwei.mvvm.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : BaseViewBindActivity<ActivityMainBinding>() {

    override fun initView(savedInstanceState: Bundle?) {

    }

    fun test(view: View) {
        CoroutineScope(Dispatchers.Main).launch {
            DataStoreUtils.saveStringData("WWW","www")
        }
        navigation(HomeActivity)
    }
}