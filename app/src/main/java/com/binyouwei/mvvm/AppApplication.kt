package com.binyouwei.mvvm

import com.binyouwei.lib_common.base.BaseApplication
import com.tencent.bugly.crashreport.CrashReport

/**
 * @author binyouwei
 * @date   2023/2/9 23:19
 * @desc   应用类
 */
class AppApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()

        // CrashReport.initCrashReport(applicationContext, "cb2d02cee6", false)
    }
}